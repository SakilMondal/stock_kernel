## ares-user 11 RP1A.200720.011 V12.5.5.0.RKJINXM release-keys
- Manufacturer: xiaomi
- Platform: mt6893
- Codename: aresin
- Brand: POCO
- Flavor: ares-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.5.0.RKJINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/aresin/aresin:11/RP1A.200720.011/V12.5.5.0.RKJINXM:user/release-keys
- OTA version: 
- Branch: ares-user-11-RP1A.200720.011-V12.5.5.0.RKJINXM-release-keys
- Repo: poco_aresin_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
